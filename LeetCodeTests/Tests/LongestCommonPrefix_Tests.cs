﻿using LeetCodeApp._014LongestCommonPrefix;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace LeetCodeTests.Tests
{
    public class LongestCommonPrefix_Tests
    {
        //public class CalculatorTestData : IEnumerable<object[]>
        //{
        //    public IEnumerator<object[]> GetEnumerator()
        //    {
        //        yield return new object[] { 1, 2, 3 };
        //        yield return new object[] { -4, -6, -10 };
        //    }

        //    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        //}

        class StringData : TheoryData<string[], string>
        {
            public StringData()
            {
                Add(new string[] { "flower", "flow", "flight" }, "fl");
                Add(new string[] { "dog", "racecar", "car" }, "");
            }
        }


        [Theory]
        [ClassData(typeof(StringData))]
        public void LongestCommonPrefixTest(string[] strs, string expected)
        {
            var longestCommonPrefix = new LongestCommonPrefixClass();
            var result = longestCommonPrefix.LongestCommonPrefix(strs);
            Assert.Equal(expected, result);
        }
    }
}
