﻿using LeetCodeApp._217ContainsDuplicate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodeTests.Tests
{
    public class ContainsDuplicate_Tests
    {
        class TestData : TheoryData<int[], bool>
        {
            public TestData()
            {
                Add(new int[] { 1, 2, 3, 1 }, true);
                Add(new int[] { 1, 2, 3, 4 }, false);
                Add(new int[] { 1, 1, 1, 3, 3, 4, 3, 2, 4, 2 }, true);
            }
        }

        [Theory]
        [ClassData(typeof(TestData))]
        public void ContainsDuplicate_Test(int[] input, bool expected)
        {
            var containsDuplicate = new ContainsDuplicateBase();
            var result = containsDuplicate.ContainsDuplicate(input);
            Assert.Equal(expected, result);
        }
        
    }
}
