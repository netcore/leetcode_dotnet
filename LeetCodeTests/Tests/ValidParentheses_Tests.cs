﻿using LeetCodeApp._020ValidParentheses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace LeetCodeTests.Tests
{
    public class ValidParentheses_Tests
    {
        class StringData : TheoryData<string, bool>
        {
            public StringData()
            {
                //Add("{[]}", true );
                //Add("(]", false);
                //Add("()", true);
                //Add("((", false);
                //Add("))", false);
                Add("([}}])", false);
            }
        }


        [Theory]
        [ClassData(typeof(StringData))]
        public void LongestCommonPrefixTest(string str, bool expected)
        {
            var validParentheses = new ValidParentheses();
            var result = validParentheses.IsValid(str);
            Assert.Equal(expected, result);
        }

        [Theory]
        [ClassData(typeof(StringData))]
        public void IsValidStackTest(string str, bool expected)
        {
            var validParentheses = new ValidParentheses();
            var result = validParentheses.IsValidStack(str);
            Assert.Equal(expected, result);
        }
    }
}
