﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodeApp._013RomanToInteger
{
    internal class ConvertRoman
    {
        Dictionary<char, int> numbers = new Dictionary<char, int>()
        {
            {'I', 1 },
            {'V',5 },
            {'X', 10 },
            {'L', 50 },
            {'C', 100 },
            {'D', 500 },
            {'M',1000 }
        };

        public int GetIntValue (string romanNumber)
        {
            int result = numbers[romanNumber[romanNumber.Length-1]];

            for (int i = romanNumber.Length-2; i >= 0 ; i--)
            {
                if (numbers[romanNumber[i]] < numbers[romanNumber[i+1]] )
                {
                    result -= numbers[romanNumber[i]];
                }
                else
                {
                    result += numbers[romanNumber[i]];
                }
            }
            return result;
        }
    }
}
