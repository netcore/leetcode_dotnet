﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodeApp._014LongestCommonPrefix
{
    public class LongestCommonPrefixClass
    {
        public string LongestCommonPrefix(string[] strs)
        {
            var result = new StringBuilder();
            var minLength = strs.Select(s => s.Length).Min();
            for (int i = 0; i < minLength; i++)
            {
                var current = strs[0][i];
                for (int j = 1; j < strs.Length; j++)
                {
                    if (current != strs[j][i])
                    {
                        return result.ToString();
                    }
                }
                result.Append(current);
            }
            return result.ToString();
        }
    }
}
