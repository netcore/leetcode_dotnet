﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodeApp._217ContainsDuplicate
{
    public class ContainsDuplicateBase
    {
        public bool ContainsDuplicate(int[] nums)
        {
            var dictInputValues = new Dictionary<int, bool>();
            foreach (int i in nums)
            {
                if (!dictInputValues.ContainsKey(i))
                {
                    dictInputValues.Add(i, true);
                    continue;
                }
                return true;
            }
            return false;
        }
    }
}
