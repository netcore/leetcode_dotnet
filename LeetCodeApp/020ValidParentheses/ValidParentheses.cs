﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodeApp._020ValidParentheses
{
    public class ValidParentheses
    {
        string[] values = new string[] { "()", "[]", "{}" };
        public bool IsValid(string s)
        {
            if(s.Length %2 ==1)
            {
                return false;
            }
            var notPairs = false;
            var strBuilder = new StringBuilder(s);
            while(strBuilder.Length!=0 && !notPairs)
            {
                notPairs = true;
                for (int i = 0; i < strBuilder.Length-1; i ++)
                {
                    var str = strBuilder.ToString(i,2);
                    if (!values.Contains(str))
                    {
                        continue;
                    }
                    notPairs = false;
                    strBuilder = strBuilder.Remove(i,2);
                    break;
                }
                //return false;
            }
           
            return !notPairs;
        }


        char[] openCharacters = new char[] { '(', '[', '{' };
        char[] closeCharacters = new char[] { ')', ']', '}' };
        Dictionary<char, char> pairs = new Dictionary<char, char>()
        {
            {'(',')' },
            {'{', '}' },
            {'[',']' }
        };
        public bool IsValidStack(string s)
        {
            if (s.Length % 2 == 1)
            {
                return false;
            }
            var stack  = new Stack<char>();

            foreach (char c in s)
            {
                if (openCharacters.Any(ch =>ch == c))
                {
                    stack.Push(c);
                    continue;
                }

                if (closeCharacters.Any(ch => ch == c))
                {
                    if (stack.Any() && pairs[stack.Peek()] == c)
                    {
                        stack.Pop();
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            return stack.Count == 0;
        }
    }
}
