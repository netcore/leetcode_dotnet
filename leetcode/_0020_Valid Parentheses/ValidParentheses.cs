﻿using System;
namespace leetcode._0020_ValidParentheses
{
    public class Solution
    {
        string[] values = new string[] { "()", "[]", "{}" };
        public bool IsValid(string s)
        {
            for (int i = 0; i < s.Length; i+=2)
            {
                var str = s[(i)..(i+2)];
                if (!values.Contains(str))
                {
                    return false;
                }
            }
            return true;
        }
    }
}

