﻿using System;
using Xunit;
using leetcode._0020_ValidParentheses;
namespace leetcode_tests.tests._0020_ValidParentheses_Tests
{
    public class ValidParenthesesTests
    {
        [Theory]
        //[InlineData("()", true)]
        [InlineData("()[]{}", true)]
        //[InlineData("(]", false)]
        public void ValidParenthesesTest(string input, bool expected)
        {
            var val = new Solution();
            var result = val.IsValid(input);
            Assert.Equal(expected, result);
        }
    }
}

